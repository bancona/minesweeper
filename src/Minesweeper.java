import java.awt.Color;
import java.awt.Dimension;
import java.awt.MouseInfo;
import javax.swing.JFrame;

public class Minesweeper extends JFrame {
	private static final long serialVersionUID = 3607472557892019201L;
	public static final int NUMCOLS = 30, NUMROWS = 16, NUMMINES = 99;
	private Tile[][] allTiles;
	private GameTimer timer;

	public static void main(String[] args) {
		Minesweeper ms = new Minesweeper("Minesweeper");
		ms.playGame();
	}

	public void playGame() {
		while (true) {
			timer.reset();
			while (Tile.gameOn) {
				// informs tile under mouse (if exists) that mouse is over it
				for (int r = 0; r < allTiles.length; r++) {
					for (int c = 0; c < allTiles[0].length; c++) {
						if (r == getMouseColumn() && c == getMouseRow()) {
							allTiles[r][c].mouseOver = true;
						} else {
							allTiles[r][c].mouseOver = false;
						}
					}
				}
				repaint();
				// timer only starts when the first Tile is clicked
				if (Tile.tileClicked) {
					timer.startTimer();
				}
				Tile.checkIfWon();
			}
			timer.stopTimer();
			RestartButton restart = new RestartButton(215, 111, this);
			getContentPane().remove(restart);
			Tile.resetTiles(allTiles);
		}
	}

	public Minesweeper(String str) {
		super(str);
		setBackground(Color.black);
		getContentPane()
				.setPreferredSize(
						new Dimension(Tile.WIDTH * NUMCOLS, Tile.HEIGHT
								* NUMROWS + 50));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		pack();
		setVisible(true);
		// add Tiles to panel
		allTiles = Tile.setUpTiles(getContentPane());
		// add MineCounter to panel
		getContentPane().add(
				new MineCounter(Tile.WIDTH * NUMCOLS / 2 + 50, Tile.HEIGHT
						* NUMROWS + 15));
		validate();
		// add GameTimer to panel
		timer = (GameTimer) getContentPane().add(
				new GameTimer(Tile.WIDTH * NUMCOLS / 2 - 80, Tile.HEIGHT
						* NUMROWS + 15));
		validate();
	}

	public int getMouseColumn() {
		return (MouseInfo.getPointerInfo().getLocation().x - getContentPane()
				.getLocationOnScreen().x) / 16;
	}

	public int getMouseRow() {
		return (MouseInfo.getPointerInfo().getLocation().y - getContentPane()
				.getLocationOnScreen().y) / 16;
	}

}