import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

public class MineCounter extends Component {
	private static final long serialVersionUID = -8833874365168332483L;
	public int x, y;
	public static final int WIDTH = 30, HEIGHT = 20;

	public MineCounter(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(x, y, WIDTH, HEIGHT);
		int mineCount = Minesweeper.NUMMINES - Tile.getNumFlags();
		int digits = ("" + mineCount).length();
		g.setColor(Color.black);
		g.drawString("" + mineCount, x + WIDTH / 2 - 4 * digits, y + HEIGHT / 2
				+ 5);
		g.drawString("" + mineCount, x + WIDTH / 2 - 4 * digits + 1, y + HEIGHT
				/ 2 + 5);
	}
}
