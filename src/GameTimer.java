import java.awt.Component;
import java.awt.Graphics;
import java.awt.Color;

public class GameTimer extends Component implements Runnable {
	private static final long serialVersionUID = 6038508523652544605L;
	private int x, y, time, width = 30, height = 20;
	public boolean timerRunning;

	public GameTimer(int x, int y) {
		this.x = x;
		this.y = y;
		new Thread(this).start();
	}

	public void run() {
		while (true) {
			if (timerRunning && time < 999) {
				time++;
			}
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			}
		}
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(x, y, width, height);
		g.setColor(Color.black);
		int digits = (time + "").length();
		g.drawString("" + time, x + width / 2 - 4 * digits, y + height / 2 + 5);
		g.drawString("" + time, x + width / 2 - 4 * digits + 1, y + height / 2
				+ 5);
	}

	public void reset() {
		time = 0;
	}

	public void stopTimer() {
		timerRunning = false;
	}

	public void startTimer() {
		timerRunning = true;
	}

}
