import java.awt.Color;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JButton;

public class RestartButton extends JButton {
	private static final long serialVersionUID = 2891199043858573705L;
	public int x, y, i;
	public final int WIDTH = 65, HEIGHT = 30, BORDER = 2;
	public boolean clicked, wasClicked;
	public Minesweeper myFrame;

	public void paint(Graphics g) {
		if (Tile.mouseLeftPressed) {
			wasClicked = true;
		}
		if (wasClicked && !Tile.mouseLeftPressed && checkMouseOver()) {
			clicked = true;
		}
		if (!Tile.mouseLeftPressed) {
			wasClicked = false;
		}
		g.setColor(Color.black);
		g.fillRect(x, y, WIDTH, HEIGHT);
		g.setColor(Color.darkGray);
		if (checkMouseOver()) {
			g.setColor(g.getColor().brighter());
		}
		g.fillRect(x + BORDER, y + BORDER, WIDTH - 2 * BORDER, HEIGHT - 2
				* BORDER);
		g.setColor(Color.white);
		g.drawString("RESTART", x + 5, y + 19);
	}

	public RestartButton(int x, int y, Minesweeper ms) {
		this.x = x;
		this.y = y;
		myFrame = ms;
		myFrame.getContentPane().add(this);
		myFrame.getContentPane().validate();
		repaint();
		while (!clicked) {
			repaint();
		}
	}

	public boolean checkMouseOver() {
		return (new Rectangle(x, y, WIDTH, HEIGHT)).contains(new Point(
				MouseInfo.getPointerInfo().getLocation().x
						- myFrame.getContentPane().getLocationOnScreen().x,
				MouseInfo.getPointerInfo().getLocation().y
						- myFrame.getContentPane().getLocationOnScreen().y));
	}

}
