import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;

public class Tile extends JButton implements MouseListener {
	private static final long serialVersionUID = -2405229892514192618L;
	public char myChar;
	public int x, y, myCol, myRow;
	public final static int WIDTH = 16, HEIGHT = 16;
	public Color tileFill = Color.gray;
	public static boolean mouseLeftPressed, mouseRightPressed,
			mouseBothPressed, gameOn = true;
	public boolean mouseOver, revealed, wasClicked, flagged, rightClicked,
			questioned, selected;
	private final Color ONE = Color.blue, TWO = Color.green, THREE = Color.red,
			FOUR = (Color.pink).darker(), FIVE = (Color.red).darker(),
			SIX = Color.cyan, SEVEN = Color.black, EIGHT = Color.darkGray;
	public static boolean tileClicked;
	public static Tile[][] theTiles = null;

	public Tile(int x, int y, char c) {
		this.x = x;
		this.y = y;
		myChar = c;
		addMouseListener(this);
		flagged = false;
	}

	public void paint(Graphics g) {
		// checks whether tile was clicked
		if (mouseLeftPressed) {
			wasClicked = true;
		}
		if (wasClicked && !mouseLeftPressed && mouseOver && !flagged
				&& !mouseBothPressed) {
			revealed = true;
			tileClicked = true;
		}
		if (!mouseLeftPressed) {
			wasClicked = false;
		}

		checkIfSelected();

		// draws border
		g.setColor(Color.black);
		g.fillRect(x, y, WIDTH, HEIGHT);

		// draws remaining tile
		if (revealed) {
			if (myChar == '*') {// if tile contains a mine
				paintMine(g);
				if (flagged) {
					// when all mines revealed, flagged mines retain their flags
					paintFlag(g);
				}
				// if any mine is revealed, all mines revealed
				blowUpAll();
			} else {// not a mine
				if (flagged) {
					// if a non-mine is flagged at game over, mine w/red X is
					// drawn
					paintMine(g);
					paintX(g);
				} else if (myChar == ' ') {
					clearAroundBlankTile();
					paintBlankTile(g);
				} else {
					paintNum(g);
				}
			}
		} else {// if tile number is still hidden
			checkForRightClick();
			paintHiddenTile(g);
			if (flagged) {
				paintFlag(g);
			}
		}

	}

	// when a hidden tile is right-clicked, it shifts from blank to flagged to
	// questioned
	private void checkForRightClick() {
		if (rightClicked) {
			rightClicked = false;
			if (mouseOver) {
				if (flagged) {
					flagged = false;
					questioned = true;
				} else if (questioned) {
					questioned = false;
				} else {
					flagged = true;
				}
			}
		}
	}

	private void checkIfSelected() {
		if (!mouseBothPressed) {
			selected = false;
			return;
		}
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				try {
					if (theTiles[myRow + i][myCol + j].mouseOver) {
						selected = true;
						return;
					}
				} catch (Exception e) {
				}
			}
		}
		selected = false;
	}

	private void paintHiddenTile(Graphics g) {
		// sets color of fill
		if (mouseOver) {
			g.setColor(tileFill.brighter());
			if (mouseLeftPressed) {
				g.setColor(Color.white);
			}
		} else {
			g.setColor(tileFill);
		}
		if (selected && !flagged) {
			g.setColor(Color.white);
		}

		// draws rectangle
		g.fillRect(x + 1, y + 1, WIDTH - 2, HEIGHT - 2);

		if (questioned) {
			paintQuestionMark(g);
		}
	}

	private void paintQuestionMark(Graphics g) {
		g.setColor(Color.black);
		g.drawString("?", x + 5, y + 13);
	}

	private void paintBlankTile(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(x + 1, y + 1, WIDTH - 2, HEIGHT - 2);
	}

	private void paintMine(Graphics g) {
		paintBlankTile(g);
		g.setColor(Color.black);
		g.fillOval(x + 2, y + 2, 11, 11);
		g.setColor(Color.white);
		g.fillRect(x + 9, y + 5, 2, 2);
	}

	private void paintNum(Graphics g) {
		paintBlankTile(g);
		Color strColor = null;
		switch (myChar) {
		case '1':
			strColor = ONE;
			break;
		case '2':
			strColor = TWO;
			break;
		case '3':
			strColor = THREE;
			break;
		case '4':
			strColor = FOUR;
			break;
		case '5':
			strColor = FIVE;
			break;
		case '6':
			strColor = SIX;
			break;
		case '7':
			strColor = SEVEN;
			break;
		case '8':
			strColor = EIGHT;
			break;
		}
		try {
			g.setColor(strColor.darker());
		} catch (Exception e) {
			g.setColor(strColor);
		}
		g.drawString("" + myChar, x + 5, y + 13);
	}

	private void paintFlag(Graphics g) {
		g.setColor(Color.red);
		Polygon flag = new Polygon();
		flag.addPoint(x + 3, y + 5);
		flag.addPoint(x + 10, y + 2);
		flag.addPoint(x + 10, y + 8);
		g.fillPolygon(flag);
		g.setColor(Color.darkGray);
		g.drawRect(x + 10, y + 2, 1, 10);
		g.drawRect(x + 6, y + 11, 6, 1);
	}

	private void paintX(Graphics g) {
		g.setColor(Color.red);
		Polygon p = new Polygon();
		p.addPoint(x + 2, y + 2);
		p.addPoint(x + 14, y + 14);
		g.drawPolygon(p);
		p = new Polygon();
		p.addPoint(x + 14, y + 2);
		p.addPoint(x + 2, y + 14);
		g.drawPolygon(p);
	}

	private void blowUpAll() {
		for (Tile[] tiles : theTiles) {
			for (Tile tile : tiles) {
				if (tile.myChar == '*' || tile.flagged) {
					tile.revealed = true;
				}
			}
		}
		gameOn = false;
	}

	private void clearAroundBlankTile() {
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				try {
					theTiles[myRow + i][myCol + j].revealed = true;
				} catch (Exception e) {
				}
			}
		}
	}

	private int getNumAdjMines() {
		int adjMines = 0;
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				try {
					if (theTiles[myRow + i][myCol + j].myChar == '*') {
						adjMines++;
					}
				} catch (Exception e) {
				}
			}
		}
		return adjMines;
	}

	private int getNumAdjFlags() {
		int flags = 0;
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				try {
					if (theTiles[myRow + i][myCol + j].flagged) {
						flags++;
					}
				} catch (Exception e) {
				}
			}
		}
		return flags;
	}

	private ArrayList<Tile> getAdjTiles() {
		ArrayList<Tile> adjTiles = new ArrayList<Tile>();
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				try {
					if (i != 0 || j != 0) {
						adjTiles.add(theTiles[myRow + i][myCol + j]);
					}
				} catch (Exception e) {
				}
			}
		}
		return adjTiles;
	}

	public void mousePressed(MouseEvent e) {
		if (!gameOn) {
			mouseLeftPressed = true;
			return;
		}
		if (e.getButton() == MouseEvent.BUTTON3)
		// right click
		{
			if (!mouseLeftPressed) {
				for (Tile[] tiles : theTiles) {
					for (Tile tile : tiles) {
						tile.rightClicked = true;
					}
				}
			}
			mouseRightPressed = true;
		} else if (e.getButton() == MouseEvent.BUTTON1) {
			mouseLeftPressed = true;
		}
		if (mouseRightPressed && mouseLeftPressed) {
			mouseBothPressed = true;
			mouseLeftPressed = false;
			mouseRightPressed = false;
		}
	}

	public void mouseReleased(MouseEvent e) {
		if (!gameOn) {
			mouseLeftPressed = false;
			return;
		}
		if (e.getButton() == MouseEvent.BUTTON1) {
			mouseLeftPressed = false;
		} else if (e.getButton() == MouseEvent.BUTTON3) {
			mouseRightPressed = false;
		}
		if (mouseBothPressed) {
			mouseBothPressed = false;
			doubleClicked();
		}
	}

	private static void doubleClicked() {
		Tile selectedTile = getSelectedTile();
		if (selectedTile == null
				|| selectedTile.flagged
				|| !selectedTile.revealed
				|| selectedTile.myChar == ' '
				|| selectedTile.getNumAdjFlags() != selectedTile
						.getNumAdjMines()) {
			return;
		}
		for (Tile tile : selectedTile.getAdjTiles()) {
			if (!tile.flagged) {
				tile.revealed = true;
			}
		}
	}

	private static Tile getSelectedTile() {
		for (Tile[] tiles : theTiles) {
			for (Tile tile : tiles) {
				if (tile.mouseOver) {
					return tile;
				}
			}
		}
		return null;
	}

	private static void setTileNumbers(Tile[][] allTiles) {
		for (Tile[] tiles : allTiles) {
			for (Tile tile : tiles) {
				tile.setNumber();
			}
		}
	}

	private void getAddress() {
		for (int r = 0; r < theTiles.length; r++) {
			for (int c = 0; c < theTiles[0].length; c++) {
				if (theTiles[r][c].equals(this)) {
					myRow = r;
					myCol = c;
					return;
				}
			}
		}
	}

	private void setNumber() {
		getAddress();
		if (myChar == '*') {
			return;
		}
		int adjMines = getNumAdjMines();
		if (adjMines == 0) {
			myChar = ' ';
		} else {
			myChar = ("" + adjMines).charAt(0);
		}
	}

	public static void resetTiles(Tile[][] allTiles) {
		for (Tile[] tiles : allTiles) {
			for (Tile tile : tiles) {
				tile.myChar = '1';
				tile.flagged = false;
				tile.mouseOver = false;
				tile.questioned = false;
				tile.rightClicked = false;
				tile.wasClicked = false;
				tile.revealed = false;
			}
		}
		addMines(allTiles);
		setTileNumbers(allTiles);
		gameOn = true;
		tileClicked = false;
	}

	public static Tile[][] setUpTiles(Container jp) {
		Tile[][] allTiles = new Tile[Minesweeper.NUMCOLS][Minesweeper.NUMROWS];

		// creates tiles
		int row = -1;
		for (int x = 0; x <= WIDTH * (Minesweeper.NUMCOLS - 1); x += WIDTH) {
			row++;
			int col = 0;
			for (int y = 0; y <= HEIGHT * (Minesweeper.NUMROWS - 1); y += HEIGHT) {
				Tile tile = new Tile(x, y, ' ');
				jp.add(tile);
				jp.validate();
				allTiles[row][col++] = tile;
			}
		}
		// makes Tile array available to all Tiles
		Tile.theTiles = allTiles;

		addMines(allTiles);
		Tile.setTileNumbers(allTiles);
		tileClicked = false;
		return allTiles;
	}

	private static void addMines(Tile[][] allTiles) {
		int mines = Minesweeper.NUMMINES;
		while (mines > 0) {
			for (Tile[] tiles : allTiles) {
				for (Tile tile : tiles) {
					if (Math.random() < (double) Minesweeper.NUMMINES
							/ (allTiles.length * allTiles[0].length)
							&& tile.myChar != '*') {
						mines--;
						tile.myChar = '*';
						if (mines <= 0) {
							return;
						}
					}
				}
			}
		}
	}

	public static int getNumMines() {
		int mines = 0;
		for (Tile[] tiles : theTiles) {
			for (Tile tile : tiles) {
				if (tile.myChar == '*') {
					mines++;
				}
			}
		}
		return mines;
	}

	public static int getNumFlags() {
		int flags = 0;
		for (Tile[] tiles : theTiles) {
			for (Tile tile : tiles) {
				if (tile.flagged) {
					flags++;
				}
			}
		}
		return flags;
	}

	public static void checkIfWon() {
		boolean won = true;
		for (Tile[] tiles : theTiles) {
			for (Tile tile : tiles) {
				if (tile.myChar == '*' && !tile.flagged) {
					won = false;
				}
			}
		}
		if (won) {
			gameOn = false;
		}
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}
}
