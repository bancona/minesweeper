# Minesweeper

#### By Alberto (Bertie) Ancona
#### Developed in spring 2013

## Overview
Minesweeper is one of a series of arcade games I built for fun during my junior year of high school. For this game, I focused on creating a clean and crisp user interface with all of the features of the Windows version of the game.

## Setup Instructions
I have included a runnable jar file that will start the game without any setup whatsoever. If you prefer, you can compile the Java files yourself by running the command `javac Minesweeper.java` and then run the program by running the command `java Minesweeper` from the [src](./src) directory.